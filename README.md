# Civo Cluster

This project help you to create and manage a Civo cluster from GitPod. It uses the **Civo CLI** to create a cluster on **[Civo](https://www.civo.com)**

## Getting started

- Set the `CIVO_API_KEY` environment variable in the GitPod's settings
- Import this project somewhere in GitLab and open it with GitPod
- 🖐️ Set the project to **private**
- Update the `.env` file with the appropriate values:

```bash
CLUSTER_NAME="panda"
CLUSTER_SIZE="g3.k3s.large"
CLUSTER_NODES=1
CLUSTER_REGION=NYC1
KUBECONFIG=./config/k3s.yaml
```

## Create the cluster

Run this script: `01-create-cluster.sh`

> `01-create-cluster.sh`
```bash
#!/bin/bash
set -o allexport; source .env; set +o allexport

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes create ${CLUSTER_NAME} --size=${CLUSTER_SIZE} --nodes=${CLUSTER_NODES} --region=${CLUSTER_REGION} --wait
```

## Get the kubeconfig file

Once the cluster is created and running, run `02-get-config.sh`, it will copy the kubeconfig file in the `./config` repository.

> `02-get-config.sh`
```bash
#!/bin/bash
set -o allexport; source .env; set +o allexport

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo --region=${CLUSTER_REGION} kubernetes config ${CLUSTER_NAME} > ./config/k3s.yaml
```

Check the configuration with these commands:

```bash
kubectl version 
kubectl get nodes
kubectl get pods --all-namespaces
```

## Run K9S

```bash
k9s --all-namespaces
```
